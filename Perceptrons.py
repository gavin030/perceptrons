"""
===========================================
Binary perceptron and multiclass perceptron
===========================================

Author: Haotian Shi, 2016

"""



import data
import math
import sys



############################################################
# Section 1: Perceptrons
############################################################

class BinaryPerceptron(object):

    def __init__(self, examples, iterations):
        if type(examples[0][0]) is dict:
            self.mod = 'dict'
        elif type(examples[0][0]) is tuple:
            self.mod = 'tuple'
        if self.mod == 'dict':
            keys = []
            for data in examples:
                keys += list(data[0])
            self.keys = sorted(set(keys))
        elif self.mod == 'tuple':
            self.keys = range(len(examples[0][0]))
        self.w = {key:0 for key in self.keys}
        self.train(examples, iterations)

    def predict(self, x):
        result = 0
        if self.mod == 'dict':
            for key in x:
                result += self.w[key] * x[key]
        elif self.mod == 'tuple':
            for key in self.keys:
                result += self.w[key] * x[key]
        return True if result > 0 else False

    def train(self, train_set, times):
        for i in range(times):
            accurate = 0
            for data in train_set:
                if self.predict(data[0]) != data[1]:
                    x = data[0]
                    if self.mod == 'dict':
                        if data[1] == True:
                            for key in x:
                                self.w[key] += x[key]
                        else:
                            for key in x:
                                self.w[key] -= x[key]
                    elif self.mod == 'tuple':
                        if data[1] == True:
                            for key in self.keys:
                                self.w[key] += x[key]
                        else:
                            for key in self.keys:
                                self.w[key] -= x[key]
                else:
                    accurate += 1
            print 'BinaryPerceptron Accuracy:', accurate / float(len(train_set))
            

class MulticlassPerceptron(object):

    def __init__(self, examples, iterations):
        if type(examples[0][0]) is dict:
            self.mod = 'dict'
        elif type(examples[0][0]) is tuple:
            self.mod = 'tuple'
        if self.mod == 'dict':
            keys = []
            for data in examples:
                keys += list(data[0])
            self.keys = sorted(set(keys))
        elif self.mod == 'tuple':
            self.keys = range(len(examples[0][0]))
        labels = set([data[1] for data in examples])
        self.w = {label:{key:0 for key in self.keys} for label in labels}
        self.train(examples, iterations)
    
    def predict(self, x):
        MAX = -sys.float_info.max
        result = None
        for l in self.w:
            product = 0
            if self.mod == 'dict':
                for key in x:
                    product += self.w[l][key] * x[key]
            elif self.mod == 'tuple':
                for key in self.keys:
                    if x[key] != 0:
                        product += self.w[l][key] * x[key]
            if product > MAX:
                MAX = product
                result = l
        return result
    
    def train(self, train_set, times):
        for i in range(times):
            accurate = 0
            for data in train_set:
                prediction = self.predict(data[0])
                if prediction != data[1]:
                    x = data[0]
                    if self.mod == 'dict':
                        for key in x:
                            self.w[data[1]][key] += x[key]
                            self.w[prediction][key] -= x[key]
                    elif self.mod == 'tuple':
                        for key in self.keys:
                            self.w[data[1]][key] += x[key]
                            self.w[prediction][key] -= x[key]
                else:
                    accurate += 1
            print 'MulticlassPerceptron Accuracy:', accurate / float(len(train_set))

############################################################
# Section 2: Applications
############################################################

class IrisClassifier(object):
    def __init__(self, data):
        iterations = 189#24
        self.perceptron = MulticlassPerceptron(data, iterations)

    def classify(self, instance):
        return self.perceptron.predict(instance)

class DigitClassifier(object):

    def __init__(self, data):
        iterations = 25
        self.perceptron = MulticlassPerceptron(data, iterations)

    def classify(self, instance):
        return self.perceptron.predict(instance)

class BiasClassifier(object):
    
    def __init__(self, data):
        self.dimension = 1
        self.bias = 0
        iterations = 5
        self.train(data, iterations)
        data = [((data[i][0] + self.bias,), data[i][1]) for i in range(len(data))]
        self.perceptron = BinaryPerceptron(data, iterations)

    def classify(self, instance):
        return self.perceptron.predict((instance + self.bias,))

    def predict(self, instance):
        return True if instance + self.bias > 0 else False

    def train(self, train_set, times):
        for i in range(times):
            accurate = 0
            for data in train_set:
                if self.predict(data[0]) != data[1]:
                    self.bias = -data[0]
                else:
                    accurate += 1
            print 'BiasClassifier Accuracy:', accurate / float(len(train_set))
    '''
    def __init__(self, data):
        print '------------BiasClassifier------------'
        self.bias = 0
        data = [({'x':data[i][0], 'bias':self.bias}, data[i][1]) for i in range(len(data))]
        iterations = 30
        self.perceptron = BinaryPerceptron(data, iterations)
    
    def classify(self, instance):
        return self.perceptron.predict({'x':instance, 'bias':self.bias})
    '''
class MysteryClassifier1(object):
    
    def __init__(self, data):
        self.dimension = 2
        self.r = 0
        iterations = 5
        self.train(data, iterations)
        data = [((data[i][0][0] ** 2 + data[i][0][1] ** 2 - self.r ** 2,), data[i][1]) for i in range(len(data))]
        self.perceptron = BinaryPerceptron(data, iterations)

    def classify(self, instance):
        return self.perceptron.predict((instance[0] ** 2 + instance[1] ** 2 - self.r ** 2,))

    def predict(self, instance):
        return True if instance[0] ** 2 + instance[1] ** 2 > self.r ** 2 else False

    def train(self, train_set, times):
        for i in range(times):
            accurate = 0
            for data in train_set:
                if self.predict(data[0]) != data[1]:
                    self.r = math.sqrt(data[0][0] ** 2 + data[0][1] ** 2)
                else:
                    accurate += 1
            print 'MysteryClassifier1 Accuracy:', accurate / float(len(train_set))
    
class MysteryClassifier2(object):

    def __init__(self, data):
        self.dimension = 3
        self.xt = 0
        self.yt = 0
        self.zt = 0
        iterations = 5
        self.train(data, iterations)
        data = [(((data[i][0][0] > self.xt) ^ (data[i][0][1] > self.yt) ^ (data[i][0][2] > self.zt),), data[i][1]) for i in range(len(data))]
        self.perceptron = BinaryPerceptron(data, iterations)

    def classify(self, instance):
        return self.perceptron.predict(((instance[0] > self.xt) ^ (instance[1] > self.yt) ^ (instance[2] > self.zt),))

    def predict(self, instance):
        return True if (instance[0] > self.xt) ^ (instance[1] > self.yt) ^ (instance[2] > self.zt) else False# xor in order to divide quadrant

    def train(self, train_set, times):
        for i in range(times):
            accurate = 0
            for data in train_set:
                if self.predict(data[0]) != data[1]:
                    self.xt = data[0][0]
                    self.yt = data[0][1]
                    self.zt = data[0][2]
                    #print [self.xt, self.yt, self.zt] # to observe the learning process
                else:
                    accurate += 1
            print 'MysteryClassifier2 Accuracy:', accurate / float(len(train_set))
                  

if __name__ == '__main__':
    train = [({"x1": 1}, True), ({"x2": 1}, True), ({"x1": -1}, False), ({"x2": -1}, False)]
    test = [{"x1": 1}, {"x1": 1, "x2": 1}, {"x1": -1, "x2": 1.5}, {"x1": -0.5, "x2": -2}]
    p = BinaryPerceptron(train, 1)
    output = [p.predict(x) for x in test]
    print output
    print output == [True, True, True, False]

    train = [({"x1": 1}, 1), ({"x1": 1, "x2": 1}, 2), ({"x2": 1}, 3), ({"x1": -1, "x2": 1}, 4), ({"x1": -1}, 5), ({"x1": -1, "x2": -1}, 6), ({"x2": -1}, 7), ({"x1": 1, "x2": -1}, 8)]
    p = MulticlassPerceptron(train, 10)
    output = [p.predict(x) for x, y in train]
    print output
    print output == [1, 2, 3, 4, 5, 6, 7, 8]

    c = IrisClassifier(data.iris)
    output = c.classify((5.1, 3.5, 1.4, 0.2))
    print output
    print output == 'iris-setosa'

    c = DigitClassifier(data.digits)
    output = c.classify((0,0,5,13,9,1,0,0,0,0,13,15,10,15,5,0,0,3,15,2,0,11,8,0,0,4,12,0,0,8,8,0,0,5,8,0,0,9,8,0,0,4,11,0,1,12,7,0,0,2,14,5,10,12,0,0,0,0,6,13,10,0,0,0))
    print output
    print output == 0

    c = BiasClassifier(data.bias)
    output = [c.classify(x) for x in (-1, 0, 0.5, 1.5, 2)]
    print output
    print output == [False, False, False, True, True]

    #find the distribution of points by drawing a 2D plot, ball
    import pylab
    x = [each[0][0] for each in data.mystery1 if each[1]==True]
    y = [each[0][1] for each in data.mystery1 if each[1]==True]
    pylab.plot(x, y, 'or-')
    x = [each[0][0] for each in data.mystery1 if each[1]==False]
    y = [each[0][1] for each in data.mystery1 if each[1]==False]
    pylab.plot(x, y, 'ob-')
    pylab.show()

    c = MysteryClassifier1(data.mystery1)
    output = [c.classify(x) for x in ((0, 0), (0, 1), (-1, 0), (1, 2), (-3, -4))]
    print output
    print output == [False, False, False, True, True]

    #find the distribution of points by drawing a 3D plot, quadrant

    from matplotlib import pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    x = [each[0][0] for each in data.mystery2 if each[1] == True]
    y = [each[0][1] for each in data.mystery2 if each[1] == True]
    z = [each[0][2] for each in data.mystery2 if each[1] == True]
    ax.scatter(x, y, z, c='r')
    x = [each[0][0] for each in data.mystery2 if each[1] == False]
    y = [each[0][1] for each in data.mystery2 if each[1] == False]
    z = [each[0][2] for each in data.mystery2 if each[1] == False]
    ax.scatter(x, y, z, c='b')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    plt.show()


    c = MysteryClassifier2(data.mystery2)
    output = [c.classify(x) for x in ((1, 1, 1), (-1, -1, -1), (1, 2, -3), (-1, -2, 3))]
    print output
    print output == [True, False, False, True]
